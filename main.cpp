
#include "view.h"

int main()
{
	// Window Initialization
	if (!cpInit(WindowTitle, WindowWidth, WindowHeight)){
		printf("Window Initialization Failed!\n");
		exit(true);
	}
	// Game Initialization
	GameInit data;
	if (data.check_Error()){
		printf("Game Initialization Failed!\n");
		fflush(stdout);
		exit(true);
	}
	// Create Menu
	Menu menu;
	// toMainMenu
	menu.mainLoop(data);

	return 0;
}
