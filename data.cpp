#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <string.h>
#include "data.h"

Event event;

// function for know What's the date today
std::string findNowTime()
{
	time_t cur;
	time(&cur);
	std::string today = ctime(&cur);
	today = today.substr(4,7) + today.substr(20,4);
	return today;
}
// include data (image/sound/font/music)
GameInit::GameInit()
{

	// font
	minimalFont = cpLoadFont("./data/font/bangna-new.ttf", 20);
	bigMinimalFont = cpLoadFont("./data/font/bangna-new.ttf", 40);
	eightBitFontS = cpLoadFont("./data/font/ZoodHarit8Bit.ttf", 40);
	eightBitFontM = cpLoadFont("./data/font/ZoodHarit8Bit.ttf", 70);
	// paddle Texture
	paddleTexture[0] = cpLoadTexture("./data/texture/paddle/paddle.png");
	paddleTexture[1] = cpLoadTexture("./data/texture/paddle/paddle2.png");
	paddleTexture[2] = cpLoadTexture("./data/texture/paddle/paddle3.png");
	paddleTexture[3] = cpLoadTexture("./data/texture/paddle/paddle4.png");
	paddleTexture[4] = cpLoadTexture("./data/texture/paddle/paddle5.png");
	// paddleMissile Texture
	missileTexture = cpLoadTexture("./data/texture/paddle/missile.png");
	// ball Texture
	ballTexture[0] = cpLoadTexture("./data/texture/ball/ball.png");
	ballTexture[1] = cpLoadTexture("./data/texture/ball/ball_point.png");
	ballTexture[2] = cpLoadTexture("./data/texture/ball/ball_invert_b.png");
	ballTexture[3] = cpLoadTexture("./data/texture/ball/ball_invert_w.png");
	// brick Texture
	// for all
	brickTexture[0] = cpLoadTexture("./data/texture/brick/brick_ubk.png"); 			//Unbreakable
	brickTexture[1] = cpLoadTexture("./data/texture/brick/brick_itemS.png");		//ItemRare
	brickTexture[2] = cpLoadTexture("./data/texture/brick/brick_itemS2.png");		//ItemRareV2
	brickTexture[3] = cpLoadTexture("./data/texture/brick/brick_itemX.png");		//GoodItem
	brickTexture[4] = cpLoadTexture("./data/texture/brick/brick_bad.png");			//BadItem
	//for lv1
	brick1Texture[0] = cpLoadTexture("./data/texture/brick/lv1/brick_lv1.png");
	brick1Texture[1] = cpLoadTexture("./data/texture/brick/lv1/brick_lv1_s1.png");
	brick1Texture[2] = cpLoadTexture("./data/texture/brick/lv1/brick_lv1_s2.png");
	//for lv2
	brick2Texture[0] = cpLoadTexture("./data/texture/brick/lv2/brick_lv2.png");
	brick2Texture[1] = cpLoadTexture("./data/texture/brick/lv2/brick_lv2_s1.png");
	brick2Texture[2] = cpLoadTexture("./data/texture/brick/lv2/brick_lv2_s2.png");
	brick2Texture[3] = cpLoadTexture("./data/texture/brick/lv2/brick_lv2_ss1.png");
	brick2Texture[4] = cpLoadTexture("./data/texture/brick/lv2/brick_lv2_ss2.png");
	brick2Texture[5] = cpLoadTexture("./data/texture/brick/lv2/brick_lv2_ss3.png");
	//for lv3
	brick3Texture[0] = cpLoadTexture("./data/texture/brick/lv3/brick_lv3.png");
	brick3Texture[1] = cpLoadTexture("./data/texture/brick/lv3/brick_lv3_s1.png");
	brick3Texture[2] = cpLoadTexture("./data/texture/brick/lv3/brick_lv3_s2.png");
	brick3Texture[3] = cpLoadTexture("./data/texture/brick/lv3/brick_lv3_ss1.png");
	brick3Texture[4] = cpLoadTexture("./data/texture/brick/lv3/brick_lv3_ss2.png");
	brick3Texture[5] = cpLoadTexture("./data/texture/brick/lv3/brick_lv3_ss3.png");
	brick3Texture[6] = cpLoadTexture("./data/texture/brick/lv3/brick_lv3_sss1.png");
	brick3Texture[7] = cpLoadTexture("./data/texture/brick/lv3/brick_lv3_sss2.png");
	brick3Texture[8] = cpLoadTexture("./data/texture/brick/lv3/brick_lv3_sss3.png");
	brick3Texture[9] = cpLoadTexture("./data/texture/brick/lv3/brick_lv3_sss4.png");
	// item Texture
	// good (3)
	itemTexture[0] = cpLoadTexture("./data/texture/item/item_invert.png"); 		// x 	//10% 0				//ball
	itemTexture[1] = cpLoadTexture("./data/texture/item/item_longerPad.png"); 	// x 	//40% 1 2 3 4		//pad
	itemTexture[2] = cpLoadTexture("./data/texture/item/item_point.png"); 		// x 	//20% 5 6			//player
	itemTexture[3] = cpLoadTexture("./data/texture/item/item_slow.png"); 		// x 	//30% 7 8 9			//ball
	// special (1,2)
	itemTexture[4] = cpLoadTexture("./data/texture/item/item_destroyer.png"); 	// s 	//30% 0 1 2			//aura2
	itemTexture[5] = cpLoadTexture("./data/texture/item/item_lifeup.png"); 		// s 	//10% 3				//player
	itemTexture[6] = cpLoadTexture("./data/texture/item/item_ghost.png"); 		// s 	//10% 4				//aura2
	itemTexture[7] = cpLoadTexture("./data/texture/item/item_x3.png"); 			// s 	//20% 5 6			//--
	itemTexture[8] = cpLoadTexture("./data/texture/item/item_revive.png"); 		// s 	//10% 7				//aura3
	itemTexture[9] = cpLoadTexture("./data/texture/item/item_missile.png"); 	// s 	//20% 8 9			//pad
	// bad (4)
	itemTexture[10] = cpLoadTexture("./data/texture/item/item_speed.png"); 		// - 	//40% 0 1 2 3		//ball
	itemTexture[11] = cpLoadTexture("./data/texture/item/item_default.png"); 	// - 	//50% 4 5 6 7 8		//ball+aura
	itemTexture[12] = cpLoadTexture("./data/texture/item/item_death.png"); 		// - 	//10% 9				//player
	// Aura 1
	ballAura[0] = cpLoadTexture("./data/texture/ball/aura_breaker.png");
	ballAura[1] = cpLoadTexture("./data/texture/ball/aura_ghost.png");
	// Aura 2
	ballAura[2] = cpLoadTexture("./data/texture/ball/aura_revive.png");
	// Background Texture
	bgStageTexture[0] = cpLoadTexture("./data/texture/background/bg_stage1.png");
	bgStageTexture[1] = cpLoadTexture("./data/texture/background/bg_stage2.png");
	bgStageTexture[2] = cpLoadTexture("./data/texture/background/bg_stage3.png");
	// Menu Texture
	bgMenuTexture = cpLoadTexture("./data/texture/menu/bg_menu.png");
	addNameTexture = cpLoadTexture("./data/texture/menu/add_name1.png");
	inputNameTexture = cpLoadTexture("./data/texture/menu/add_name2.png");
	commitNameTexture = cpLoadTexture("./data/texture/menu/add_name3.png");
	cancelNameTexture = cpLoadTexture("./data/texture/menu/add_name4.png");
	bgAddNameTexture = cpLoadTexture("./data/texture/menu/bg_menu_an.png");
	satinTexture = cpLoadTexture("./data/texture/menu/satin.png");
	// Selection
	selectionTexture[0] = cpLoadTexture("./data/texture/menu/unselect.png");
	selectionTexture[1] = cpLoadTexture("./data/texture/menu/select.png");
	// First Page
	gameFadeTexture = cpLoadTexture("./data/texture/firstPage/nameFade.png");
	gameNameTexture = cpLoadTexture("./data/texture/firstPage/nameGame.png");
	// Press Enter
	pressEnterFadeTexture = cpLoadTexture("./data/texture/firstPage/pressEnterFade.png");
	pressEnter[0] = cpLoadTexture("./data/texture/firstPage/pressEnter.png");
	pressEnter[1] = cpLoadTexture("./data/texture/firstPage/pressEnter2.png");
	// Sound
	missileSound = cpLoadSound("./data/sound/missile2.wav");//
	brickBreakSound = cpLoadSound("./data/sound/brickBreak.wav");//
	brickCollideSound[0] = cpLoadSound("./data/sound/hitBrick1.wav");//
	brickCollideSound[1] = cpLoadSound("./data/sound/hitBrick2.wav");//
	brickCollideSound[2] = cpLoadSound("./data/sound/hitBrick3.wav");//
	brickCollideSound[3] = cpLoadSound("./data/sound/hitBrick4.wav");//
	wallCollideSound = cpLoadSound("./data/sound/hitWall.wav");//
	ubkCollideSound = cpLoadSound("./data/sound/hitUbk.wav");//
	WoodBreakSound = cpLoadSound("./data/sound/WoodBreak.wav");//
	selectorSound = cpLoadSound("./data/sound/selector.wav");//
	padCollideSound = cpLoadSound("./data/sound/hitPad.wav");//
	itemGetSound = cpLoadSound("./data/sound/itemGet.wav");//
	enterSound = cpLoadSound("./data/sound/enter.wav");//
	keySound =  cpLoadSound("./data/sound/key.wav");//
	cancelSound = cpLoadSound("./data/sound/cancel.wav");//
	mBombSound = cpLoadSound("./data/sound/rock.wav");//
	glassBreakSound = cpLoadSound("./data/sound/glassBreak3.wav");//
	finishStageSound = cpLoadSound("./data/sound/finish.wav");//
	toGameSound = cpLoadSound("./data/sound/start.wav");//
	deadSound = cpLoadSound("./data/sound/dead2.wav");//
	// Music
	bgm = cpLoadMusic("./data/pokemonMusic.mp3");
	// set Music/Sound Allow
	sound = true;
	music = true;
}
// Check Input file Error
bool GameInit::check_Error()
{
	// check font
	if (!this->minimalFont || !this->bigMinimalFont ||
		!this->eightBitFontS || !this->eightBitFontM)
		return false;
	// check ball
	for (int i = 0; i < 4; i++){
		if (!this->ballTexture[i]){
			printf("ball Error!");
			return true;
		}
	}
	// check paddle
	for (int i = 0; i < 5; i++){
		if (!this->paddleTexture[i]){
			printf("Paddle Error!");
			return true;
		}
	}
	// check Missile
	if (!this->missileTexture){
		printf("missile Error!");
		return true;
	}
	// check brick (item and Unbreakable)
	for (int i = 0; i < 5; i++){
		if (!this->brickTexture[i]){
			printf("brick Error!");
			return true;
		}
	}
	// check brick lv1
	for (int i = 0; i < 3; i++){
		if (!this->brick1Texture[i]){
			printf("brick Error!");
			return true;
		}
	}
	// check brick lv2
	for (int i = 0; i < 6; i++){
		if (!this->brick2Texture[i]){
			printf("brick Error!");
			return true;
		}
	}
	// check brick lv3
	for (int i = 0; i < 10; i++){
		if (!this->brick3Texture[i]){
			printf("brick Error!");
			return true;
		}
	}
	// check item
	for (int i = 0; i < 13; i++){
		if (!this->itemTexture[i]){
			printf("item Error!");
			return true;
		}
	}
	// check ball aura
	for (int i = 0; i < 3; i++){
		if (!this->ballAura[i]){
			printf("Aura Error!");
			return true;
		}
	}
	// check bgStage
	for (int i = 0; i < 3; i++){
		if (!this->bgStageTexture[i]){
			printf("bg Error!");
			return true;
		}
	}
	// check more Texture
	if (!this->bgMenuTexture || !this->addNameTexture ||
	    !this->inputNameTexture || !this->commitNameTexture ||
	    !this->cancelNameTexture || !this->satinTexture ||
	    !this->bgAddNameTexture || !this->selectionTexture[0] ||
	    !this->selectionTexture[1] || !this->gameFadeTexture ||
 	    !this->gameNameTexture || !this->pressEnterFadeTexture ||
 	    !this->pressEnter[0] || !this->pressEnter[1]){
			printf("Texture-Error");
			return true;
	}
	// check sound
	if (!this->missileSound || !this->brickBreakSound ||
	    !this->brickCollideSound[0] || !this->brickCollideSound[1] ||
	    !this->brickCollideSound[2] || !this->brickCollideSound[3] ||
	    !this->wallCollideSound || !this->ubkCollideSound ||
	    !this->WoodBreakSound  || !this->padCollideSound ||
	    !this->selectorSound || !this->itemGetSound ||
	    !this->enterSound || !this->keySound ||
	    !this->cancelSound || !this->mBombSound ||
	    !this->glassBreakSound || !this->finishStageSound ||
	    !this->toGameSound || !this->deadSound){
			printf("Soud-Error");
			return false;
	}
	// check music
	if (this->bgm){
		printf("Music-Error");
		return true;
	}
	return false;
}
// Player Constructor
Player::Player()
{
	score = 0;
	life = 3;
	mode = 0;
	startAt = 1;
	padSkin	= 0;
	difficultyLevel = 1;
	pointMultiple = 1;
	revivable = false;
	highestSpd = 0;
	name[0] = '\0';
}
// Add Player's Name before start game
bool Player::addName(GameInit data)
{
	bool running = true, shift = false;
	int choice = 1;
	int namePos = 0;
	// Add Name Window Pop!
	this->addNamePop(data);
	// Wait for Key name and confirm
	while (running){
		// Clear Screen
		cpClearScreen();
		// Draw background
		cpDrawTexture(255, 255, 255, 0, 0,
		 WindowWidth, WindowHeight, data.bgAddNameTexture);
		// Draw Add Name Window
		if (choice == 1){
			cpDrawTexture(255, 255, 255, 208, 192,
			 384, 216, data.inputNameTexture);
		}
		else if (choice == 2){
			cpDrawTexture(255, 255, 255, 208, 192,
			 384, 216, data.commitNameTexture);
		}
		else if (choice == 3){
			cpDrawTexture(255, 255, 255, 208, 192,
			 384, 216, data.cancelNameTexture);
		}
		// Draw Text in Add Name Window
		cpDrawText(255, 255, 255, 410, 230, "WHO ARE YOU", data.eightBitFontM, 1);
		cpDrawText(choice==1?0:255, 255, 255, 410, 305, this->name, data.eightBitFontS, 1);
		// get Event
		while (cbPollEvent(&event)){
			// exit
			if (event.type == QUIT)
				exit(true);
			// shift for UpperAlphabet
			else if (event.type == KEYDOWN && (event.key.keysym.sym == K_RSHIFT || event.key.keysym.sym == K_LSHIFT))
				shift = true;
			else if (event.type == KEYUP){
				// Change Choice
				if (event.key.keysym.sym == K_UP && choice != 1){
					if (data.sound)
						cpPlaySound(data.selectorSound);
					choice = 1;
				}
				else if (event.key.keysym.sym == K_DOWN){
					if (choice == 1){
						if (data.sound)
							cpPlaySound(data.selectorSound);
						choice = 2;
					}
				}
				else if (event.key.keysym.sym == K_LEFT && choice != 2){
					if (data.sound)
						cpPlaySound(data.selectorSound);
					choice = 2;
				}
				else if (event.key.keysym.sym == K_RIGHT && choice != 3){
					if (data.sound)
						cpPlaySound(data.selectorSound);
					choice = 3;
				}
				// Enter
				else if (event.key.keysym.sym == K_RETURN){
					if (choice == 1){
						if (data.sound)
							cpPlaySound(data.selectorSound);
						choice = 2;
					}
					// Confirm and start game
					else if (choice == 2){
						if (data.sound)
							cpPlaySound(data.enterSound);
						return 1;
					}
					// Cancel back to mainMenu
					else if (choice == 3){
						if (data.sound)
							cpPlaySound(data.cancelSound);
						return 0;
					}
				}
				// getname
				// erase word
				else if (event.key.keysym.sym == K_BCKSPC && namePos > 0){
					if (data.sound)
						cpPlaySound(data.keySound);
					this->name[--namePos] = '\0';
				}
				// write word
				else if (event.key.keysym.sym == K_RSHIFT || event.key.keysym.sym == K_LSHIFT)
					shift = false;
				else if (shift && event.key.keysym.sym >= 'a' && event.key.keysym.sym <= 'z'){
					if (data.sound)
						cpPlaySound(data.keySound);
					this->name[namePos++] = event.key.keysym.sym-32;
					this->name[namePos] = '\0';
				}
				else if (choice == 1 && namePos < 20 && event.key.keysym.sym >=32 && event.key.keysym.sym <= 126){
					if (data.sound)
						cpPlaySound(data.keySound);
					this->name[namePos++] = event.key.keysym.sym;
					this->name[namePos] = '\0';
				}
			}
		}
		// update
		cpSwapBuffers();
	}
}
void Player::addNamePop(GameInit data)
{
	// PopUp add name window
	for (float i = 0; i < 30; i++){
		cpClearScreen();
		cpDrawTexture(255, 255, 255, 0, 0,
		 WindowWidth, WindowHeight, data.bgAddNameTexture);
		cpDrawTexture(255, 255, 255, 208+192*(30-i)/30,
		 192+108*(30-i)/30, 384*i/30, 216*i/30, data.addNameTexture);
		cpDelay(20);
		cpSwapBuffers();
	}
}
void Player::die()
{
	// if dont have rerivable aura = dead
	if (!this->revivable)
		this->life--;
	// revive
	else
		this->revivable = false;
}
bool Player::lose(GameInit data)
{
	// life = 0 , end game
	cpDrawText(255, 255, 0, WindowWidth/2, WindowHeight*2/3,
	 "END GAME", data.bigMinimalFont, 1);
	cpSwapBuffers();
	// wait for add new highscore and back to main menu
	while(1){
		while(cbPollEvent(&event)){
			if (event.type == QUIT || event.type == M_UP || event.type == KEYUP &&
			 (event.key.keysym.sym == K_ESCAPE || event.key.keysym.sym == K_RETURN)){
				this->addNewHighScore();
				return false;
			}
		}
	}
}
void Player::toDefault()
{
	this->life = 3;
	this->score = 0;
	this->difficultyLevel = 1;
	this->pointMultiple = 1;
	this->revivable = false;
	this->highestSpd = 0;
}
void Player::addNewHighScore()
{
	// Array for storing top10 and new one
    Eplayer ePlayer[11];

	// load data (top10)
    FILE* board = fopen("./data/scrBoard","r");
    for (int i = 0; i < 11; i++){
    	fscanf(board,"%[^\t]\t%d\t%f\t%[^\n]\n",
    	 &(ePlayer[i].name), &(ePlayer[i].score), &(ePlayer[i].speed), &(ePlayer[i].timePlay));
    }
    fclose(board);
    // new player add to array
    strcpy(ePlayer[10].name, this->name[0]=='\0'? "Anonymous":this->name);
    ePlayer[10].score = this->score;
    ePlayer[10].speed = this->highestSpd;
    char* temp = const_cast<char*>(findNowTime().c_str());
    memcpy(ePlayer[10].timePlay, temp, 12);

    // sorting
    for (int i = 10; i > 0; i--){
		if (ePlayer[i].score > ePlayer[i-1].score){
			Eplayer temp;
			temp = ePlayer[i];
			ePlayer[i] = ePlayer[i-1];
			ePlayer[i-1] = temp;
		}
		else
			break;
    }

   	// Rewrite
	board = fopen("./data/scrBoard","w");
    // Add new ScoreBoard to File (only top ten)
    for (int i = 0; i < 10; i++){
    	fprintf(board, "%s\t%d\t%.1f\t%s\n",
    	 ePlayer[i].name, ePlayer[i].score, ePlayer[i].speed, ePlayer[i].timePlay);
    }
    fclose(board);
}
void Player::getSettingData(int settingData[])
{
	// set data from setting
	this->mode = settingData[0];
	this->difficultyLevel = settingData[1];
	this->startAt = settingData[2]+1;
	this->padSkin = settingData[3];
}

Missile::Missile()
{
	fired = false;
}
int Missile::shoot(Obj bricks[], int nBricks, GameInit data, Player* player)
{
	// fired maximum = 10 in same time,
	// but in fact we can fired only 6 missile at same time
	int cnt = 0;
	for (int i = 0; i < 10; i++){
		if (this[i].fired){
			this[i].y -= this[i].velY;
			if (int c = this[i].collide(bricks, nBricks)){
				if (data.sound)
					cpPlaySound(data.mBombSound);
				cnt += c-1;
				this[i].fired = false;
			}
		}
	}
	// plus point per hit
	player->score += 5*cnt;
	return cnt;
}
int Missile::collide(Obj bricks[], int nBricks)
{
	int cnt = 0;
	// hit the ceiling
	if (this->y < 25)
		return 1;
	// check hit bricks
	for (int i = nBricks-1; i >= 0 ; i--){
		if (this->y <= bricks[i].y + bricks[i].height && bricks[i].form+1 &&
		 (this->x + this->width > bricks[i].x && this->x <= bricks[i].x ||
	 	 bricks[i].x + bricks[i].width > this->x && bricks[i].x <= this->x)){
			// hit Unbreakable brick
	 	 	if (bricks[i].form == 0)
	 	 		return 1;
			// hit and break brick
			else if (bricks[i].form <=5 || bricks[i].form == 7 ||
			 bricks[i].form == 10 || bricks[i].form == 14){
				cnt++;
				bricks[i].form = -1;
			}
			// hit the brick that you have to hit more than 1 time.
			else
				bricks[i].form++;
			return ++cnt;
		}
	}
	return cnt;
}

Paddle::Paddle()
{
	x = defaultX;
	y = defaultY;
	width = defaultWidth;
	velX = 0;
	missileCoolDown = 0;
	missileActivate = false;
	missileSupply = 2;
}
void Paddle::toDefault()
{
	this->x = this->defaultX;
	this->y = this->defaultY;
	this->width = this->defaultWidth;
	this->velX = 0;
	this->missileActivate = false;
}
void Paddle::move(Player player)
{
	// Keyboard mode
	if (!player.mode){
		this->x  = this->x + this->velX;
		if (this->x < 25)
			this->x = 25;
		else if (this->x + this->width + 25 > WindowWidth)
			this->x = WindowWidth - this->width - 25;
	}
	// Mouse mode
	else{
		this->x = event.motion.x - this->width/2;
		if (this->x < 25)
			this->x = 25;
		else if (this->x + this->width > WindowWidth - 25)
			this->x = WindowWidth - 25 - this->width;
	}
}
void Paddle::getEvent(Missile missile[], GameInit data)
{
	// control paddle vel x
	if (event.type == KEYDOWN){
		// move
		if (event.key.keysym.sym == K_LEFT)
			this->velX = -this->defaultVelX;
		if (event.key.keysym.sym == K_RIGHT)
			this->velX = this->defaultVelX;
	}
	else if (event.type == KEYUP){
		// stop
		if (event.key.keysym.sym == K_LEFT && this->velX < 0)
			this->velX = 0;
		else if (event.key.keysym.sym == K_RIGHT && this->velX > 0)
			this->velX = 0;
		// shoot missile
		else if (event.key.keysym.sym == K_SPACE)
			this->shootMissile(missile, data);
	}
}
void Paddle::shootMissile(Missile missile[], GameInit data)
{
	// check missile activate and cooldown
	if (this->missileActivate && !this->missileCoolDown && this->missileSupply == 2){
		this->missileCoolDown = 40;
		// shoot missile
		for (int i = 0; i < 10; i++){
			if (!missile[i].fired){
				missile[i].y = this->y - 10;
				if (this->missileSupply == 2){
					missile[i].x = this->x;
					missile[i].fired = true;
					this->missileSupply--;
				}
				else if (this->missileSupply == 1){
					if (data.sound)
						cpPlaySound(data.missileSound);
					missile[i].x = this->x + this->width - missile[i].width;
					missile[i].fired = true;
					this->missileSupply--;
				}
			}
		}
	}
}
void Paddle::reloadMissile()
{
	// if Complete CoolingDown, Missile Reload
	if (this->missileCoolDown)
		this->missileCoolDown--;
	if (!this->missileCoolDown)
		this->missileSupply = 2;
}

Ball::Ball()
{
	x = defaultX;
	y = defaultY;
	width = defaultWidth;
	height = defaultHeight;
	velR = defaultVelR;
	velY = 7;
	velX = 1;
	type = 0;
	destroyed = true;
}
void Ball::beDestroyed()
{
	// ball hit the floor
	for (int i = 0; i < 35; i++){
		if (!this[i].destroyed){
			if (this[i].y + this[i].height > WindowHeight){
				this[i].destroyed = true;
			}
		}
	}
}
void Ball::toDefault()
{
	this->x = this->defaultX;
	this->y = this->defaultY;
	this->width = this->defaultWidth;
	this->height = this->defaultHeight;
	this->type = 0;
	this->velR = this->defaultVelR;
	this->velY = 7;
	this->velX = 0;
	this->destroyed = false;
	int dfAura[3] = {0,0,0};
	memcpy(this->aura, dfAura, sizeof(dfAura));
}
void Ball::collideWall(GameInit data)
{
	// hit left wall
	if (this->x <= 25){
		if (data.sound)
			cpPlaySound(data.wallCollideSound);
		this->velX = fabs(this->velX);
		this->typeInvert();
		this->ghostReform();
		if (this->velX < 1 && this->velX > -1)
			this->velX = 5;
	}
	// hit right wall
	else if (this->x + this->width + 25 >= WindowWidth){
		if (data.sound)
			cpPlaySound(data.wallCollideSound);
		this->velX = -fabs(this->velX);
		this->typeInvert();
		this->ghostReform();
		if (this->velX < 1 && this->velX > -1)
			this->velX = -5;
	}
	// hit ceiling
	if (this->y < 25){
		if (data.sound)
			cpPlaySound(data.wallCollideSound);
		this->velY = fabs(this->velY);
		this->typeInvert();
		this->ghostReform();
		if (this->velY == 0)
			this->velY = 2;
	}
	this->limitSpeed();
}
void Ball::move(Player* player)
{
	for (int i = 0; i < 35; i++){
		if (!this[i].destroyed){
			// Move
			this[i].x += this[i].velX;
			this[i].y += this[i].velY;

			// is it MaxSpd?
			if (this[i].velR > player->highestSpd)
				player->highestSpd = this[i].velR;
		}
	}
}
void Ball::collidePaddle(GameInit data, Paddle paddle)
{
	// check collide
	if (this->y + this->height >= paddle.y &&
	 (this->x + this->width > paddle.x && this->x <= paddle.x ||
	 paddle.x + paddle.width > this->x && paddle.x <= this->x)){
	 	this->typeInvert();
		// calculate ball movement direction
		this->velX = this->velR*2*(this->x + (this->x < paddle.x? this->width:0) -
		 paddle.x - paddle.width/2) / paddle.width;
		this->velY = -sqrt(pow(this->velR, 2) - pow(this->velX, 2));
		// hit paddle sound
		if (data.sound)
			cpPlaySound(data.padCollideSound);
	}
	this->limitSpeed();
}
int Ball::collideBrick(Obj bricks[], int nBricks, Player *player, int cItemDrop[][2], GameInit data)
{
	bool minus;
 	int cnt = 0;
 	for (int i = 0; i < nBricks; i++){
 		minus = false;
 		if (bricks[i].form+1){
 			// calculate movement reflect
 			if ((this->y <= bricks[i].y + bricks[i].height && this->y - this->velY >= bricks[i].y + bricks[i].height ||
 			 bricks[i].y <= this->y + this->height && bricks[i].y >= this->y + this->height - this->velY)&&
 			 (this->x + this->width > bricks[i].x && this->x <= bricks[i].x || this->x < bricks[i].x + bricks[i].width && this->x >= bricks[i].x)){
 				this->velY = -this->velY;
 				this->typeGhost(this->velY);
 				minus = true;
 			}
 			if ((this->x <= bricks[i].x + bricks[i].width && this->x - this->velX >= bricks[i].x + bricks[i].width ||
 			 bricks[i].x <= this->x + this->width && bricks[i].x >= this->x + this->width - this->velX) &&
 			 (this->y + this->height > bricks[i].y && this->y <= bricks[i].y || this->y < bricks[i].y + bricks[i].height && this->y >= bricks[i].y)){
				this->velX = -this->velX;
 				this->typeGhost(this->velX);
 				minus = true;
 			}
 			this->limitSpeed();
 			//change form
 			if (minus){
 				this->typeDestroyer(&bricks[i]);
 				this->typeInvert();
				// hit item brick
 				if (bricks[i].form <= 4 && bricks[i].form >= 1){
 					int cnt;
 					for (cnt = 0; cItemDrop[cnt][0] != -1 && cnt < 7; cnt++);
 					if (!(cItemDrop[cnt][0]+1)){
 						cItemDrop[cnt][0] = i;
 						cItemDrop[cnt][1] = bricks[i].form;
 					}
 				}
				// hit Unbreakable brick
 				if (bricks[i].form == 0){
 					this->velY*=1.1;
 					if (this->velY >= this->velR || this->velY <= -this->velR)
 						this->velY = (this->velY >= this->velR?this->velR:-this->velR)*0.8;
 					if (this->velX >= 0)
		 				this->velX = sqrt(pow(this->velR,2) - pow(this->velY,2));
		 			if (this->velX < 0)
		 				this->velX = -sqrt(pow(this->velR,2) - pow(this->velY,2));
 					if (data.sound)
 						cpPlaySound(data.ubkCollideSound);
 					player->score += 1;
 				}
				// destroy brick
 				else if (bricks[i].form <=5 || bricks[i].form == 7 || bricks[i].form == 10 || bricks[i].form == 14){
 					this->velY = this->velY*(1+(25-this->velR)/2000);
					this->velX = this->velX*(1+(25-this->velR)/2000);
					this->velR = sqrt(pow(this->velY,2) + pow(this->velX,2));
 					if (bricks[i].form == 5 && data.sound)
 						cpPlaySound(data.WoodBreakSound);
 					else if (bricks[i].form >= 5 && data.sound)
 						cpPlaySound(data.brickCollideSound[1]);
 					else if (bricks[i].form == 3 && data.sound)
 						cpPlaySound(data.glassBreakSound);
 					else if (data.sound)
 						cpPlaySound(data.brickBreakSound);
 					player->score += 15*(player->pointMultiple);
 					bricks[i].form = -1;
 					cnt++;
 				}
				// hit brick that have to hit more than 1 time
 				else{
 					this->velY = this->velY*(1+(25-this->velR)/2000);
					this->velX = this->velX*(1+(25-this->velR)/2000);
					this->velR = sqrt(pow(this->velY,2) + pow(this->velX,2));
 					if (data.sound && (bricks[i].form == 6 || bricks[i].form == 9 || bricks[i].form == 13))
 						cpPlaySound(data.brickCollideSound[2]);
 					else if (data.sound && (bricks[i].form == 8 || bricks[i].form == 12))
 						cpPlaySound(data.brickCollideSound[0]);
 					else if (data.sound && bricks[i].form == 11)
 						cpPlaySound(data.brickCollideSound[3]);
 					player->score += (bricks[i].form)*(player->pointMultiple);
 					bricks[i].form++;
 				}
 			}
 		}
 	}
 	return cnt;
}
void Ball::typeInvert()
{
	//Switch between Fast and Slow Mode
	if (this->type == 2){
		this->velY*=1.2;
		this->velX*=1.2;
		this->velR*=1.44;
		this->type = 3;
	}
	else if (this->type == 3){
		this->velY/=1.2;
		this->velX/=1.2;
		this->velR/=1.44;
		this->type = 2;
	}
}
void Ball::typeDestroyer(Obj* brick)
{
	// Can Destroy Any Brick With One Hit
	if (this->aura[0] == 1 || this->aura[0] == 2){
		if (brick->form > 5 && brick->form != 7 && brick->form != 10 && brick->form != 14){
			brick->form = 5;
		}
	}
}
int Ball::typeGhost(float vel)
{
	// Can Through All Brick But GhostBall Will Reform If It Collide Wall
	if (this->aura[0] == 2){
		if (vel == this->velY)
			this->velY = -this->velY;
		else if (vel == this->velX)
			this->velX = -this->velX;
	}
}
void Ball::ghostReform()
{
	// Back to Normal Ball
	if (this->aura[0] == 2)
		this->aura[0] = 0;
}
void Ball::multipleX3()
{
	int base[35];
	for (int i = 0; i < 35; i++){
		if (!this[i].destroyed){
			base[i] = i;
		}
		else
			base[i] = -1;
	}
	for (int i = 0; i < 35; i++){
		if (base[i]+1){
			int n = 2;
			// separate to be 3 ball
			for (int j = 0; j < 35 && n; j++){
				if (this[j].destroyed && n==2){
					this[j].destroyed = false;
					this[j].x = this[base[i]].x;
					this[j].y = this[base[i]].y;
					this[j].velX = this[base[i]].velX*1.1;
					this[j].velY = this[base[i]].velY/1.1;
					this[j].velR = this[base[i]].velR;
					this[j].type = this[base[i]].type;
					memcpy(this[j].aura,this[base[i]].aura, sizeof(this[j].aura));
					n--;
				}
				if (this[j].destroyed && n==1){
					this[j].destroyed = false;
					this[j].x = this[base[i]].x;
					this[j].y = this[base[i]].y;
					this[j].velX = this[base[i]].velX/1.1;
					this[j].velY = this[base[i]].velY*1.1;
					this[j].velR = this[base[i]].velR;
					this[j].type = this[base[i]].type;
					memcpy(this[j].aura,this[base[i]].aura, sizeof(this[j].aura));
					n--;
				}
			}
		}
	}
}
void Ball::limitSpeed()
{
	// make velR dont be higher than 25
	if (this->velR > this->maxVelR){
		this->velY = this->maxVelR*this->velY/this->velR;
		this->velX = this->maxVelR*this->velX/this->velR;
		this->velR = this->maxVelR;
	}
}

Item::Item()
{
	x = 0;
	y = 0;
	type = 0;
	created = false;
}
void Item::drop(GameInit data, Paddle* paddle, Ball* ball, Player* player)
{
	// item fall
	this->y += this->VelY;
	// player take
	if (this->y + this->height >= paddle->y &&
	 (this->x + this->width > paddle->x && this->x <= paddle->x ||
	 paddle->x + paddle->width > this->x && paddle->x <= this->x)){
	 	if (data.sound)
		 	cpPlaySound(data.itemGetSound);
	 	this->beTaken(paddle, ball, player);
		this->created = false;
	}
	// player don't take, be destroyed
	if (this->y + this->height > WindowHeight)
		this->created = false;
}
void Item::randomType()
{
	// random Item Drop
	int chanceCalc;
	chanceCalc = rand()%10;
	//good item
	if (this->type == 3){
		if (chanceCalc == 0)
			this->type = 0;
		else if (chanceCalc >= 1 && chanceCalc <= 4)
			this->type = 1;
		else if (chanceCalc == 5 || chanceCalc == 6)
			this->type = 2;
		else if (chanceCalc >=7)
			this->type = 3;
	}
	// special item
	else if (this->type == 2 || this->type == 1){
		if (chanceCalc <= 2)
			this->type = 4;
		else if (chanceCalc == 3)
			this->type = 5;
		else if (chanceCalc == 4)
			this->type = 6;
		else if (chanceCalc == 5 || chanceCalc == 6)
			this->type = 7;
		else if (chanceCalc == 7)
			this->type = 8;
		else if (chanceCalc >= 8)
			this->type = 9;
	}
	//bad
	else if (this->type == 4){
		if (chanceCalc <=3)
			this->type = 10;
		else if (chanceCalc >= 4 && chanceCalc <= 8)
			this->type = 11;
		else if (chanceCalc == 9)
			this->type = 12;
	}
}
void Item::beTaken(Paddle* paddle, Ball* ball, Player* player)
{
	switch (this->type){
		case 0: // invert
			for (int i = 0; i < 35; i++){
				if (!ball[i].destroyed){
					ball[i].velR*=1.21;
					ball[i].velX*=1.1;
					ball[i].velY*=1.1;
					ball[i].type = 2;
				}
			}
			break;
		case 1: // Longer Pad
			paddle->width*=1.1;
			break;
		case 2: // Point Multiple
			for (int i = 0; i < 35; i++){
				if (!ball[i].destroyed){
					ball[i].type = 1;
				}
			}
			player->pointMultiple*=1.1;
			break;
		case 3: // Slow Ball
			for (int i = 0; i < 35; i++){
				if (!ball[i].destroyed){
					ball[i].velR*=0.9;
				}
			}
			break;
		case 4: // Destroyer
			for (int i = 0; i < 35; i++){
				if (!ball[i].destroyed){
					ball[i].aura[0] = 1;
				}
			}
			break;
		case 5: // Life++
			player->life++;
			break;
		case 6: // Ghost Ball
			for (int i = 0; i < 35; i++){
				if (!ball[i].destroyed){
					ball[i].aura[0] = 2;
				}
			}
			break;
		case 7: // MultiBall*
			ball[0].multipleX3();
			break;
		case 8: // Revivable Ball
			player->revivable = true;
			for (int i = 0; i < 35; i++){
				if (!ball[i].destroyed){
					ball[i].aura[1] = 1;
				}
			}
			break;
		case 9: // Missile Pad*
			paddle->missileActivate = true;
			break;
		case 10: // Fast Ball
			for (int i = 0; i < 35; i++){
				if (!ball[i].destroyed){
					ball[i].velR*=1.15;
				}
			}
			break;
		case 11: // Default Ball
			for (int i = 0; i < 35; i++){
				if (!ball[i].destroyed){
					ball[i].aura[0] = 0;
					ball[i].aura[1] = 0;
					ball[i].aura[2] = 0;
					ball[i].type = 0;
				}
			}
			paddle->width = paddle->defaultWidth;
			break;
		case 12: // Death Ball
			player->life-=1;
			break;
	}
}
