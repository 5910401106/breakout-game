#ifndef __GAME_DATA__
#define __GAME_DATA__

#include <string>
#include "cp_functions.h"

#define WindowTitle "Breakout"
#define WindowWidth 800
#define WindowHeight 700

// for Object that dont have method like "bricks"
typedef struct _obj
{
	float x, y;
	float width, height;
	int destroyed;
	int form;
}Obj;

// Struct About ScoreBoard
typedef struct _ePlayer
{
	char name[20];
	int score;
	float speed;
	char timePlay[20];
} Eplayer;

std::string findNowTime();

// Import Texture(picture), Font and Sound
class GameInit
{
public:
	//Texture Initialization
	Texture ballTexture[4];
	Texture paddleTexture[5];
	Texture missileTexture;
	Texture brickTexture[5];
	Texture brick1Texture[3];
	Texture brick2Texture[6];
	Texture brick3Texture[10];
	Texture itemTexture[13];
	Texture bgStageTexture[3];
	Texture bgMenuTexture, satinTexture, bgAddNameTexture;
	Texture addNameTexture, inputNameTexture, commitNameTexture, cancelNameTexture;
	Texture selectionTexture[2];
	Texture ballAura[3];
	Texture gameFadeTexture, gameNameTexture;
	Texture pressEnter[2];
	Texture pressEnterFadeTexture;
	//Font Initialization
	Font minimalFont, bigMinimalFont;
	Font eightBitFontS, eightBitFontM;
	// Sound Initialization (Ver1)
	Sound missileSound;
	Sound brickBreakSound;
	Sound brickCollideSound[4];
	Sound wallCollideSound;
	Sound ubkCollideSound;
	Sound padCollideSound;
	Sound itemGetSound;
	Sound enterSound;
	Sound keySound;
	Sound cancelSound;
	Sound mBombSound;
	Sound glassBreakSound;
	Sound selectorSound;
	Sound WoodBreakSound;
	Sound finishStageSound;
	Sound toGameSound;
	Sound deadSound;

	// Music
	Music bgm;

	int sound, music;

	GameInit();
	bool check_Error();
};

// Player (User)
class Player
{
public:
	int score;
	int life;
	int mode, difficultyLevel, startAt, padSkin;
	bool sound;
	bool revivable;
	char name[20];
	float highestSpd;
	float pointMultiple;
	Player();
	bool lose(GameInit data);
	bool addName(GameInit data);
	void addNamePop(GameInit data);
	void die();
	void toDefault();
	void addNewHighScore();
	void getSettingData(int settingData[]);
};

// Missile From Paddle, be Activate When Paddle get MissileItem
class Missile
{
public:
	const int width = 20;
	const int height = 50;
	const float velY = 5;
	float x, y;
	bool fired;
	Missile();
	int shoot(Obj bricks[], int nBricks, GameInit data, Player* player);
	int collide(Obj bricks[], int nBricks);
};

class Paddle
{
public:
	//attribute
	const float defaultWidth = 150;
	const float height = 35;
	float x, y;
	float width;
	bool missileActivate;
	int missileCoolDown;
	int missileSupply;
	int skin;
	//behavior
	Paddle();
	void toDefault();
	void move(Player player);
	void getEvent(Missile missile[], GameInit data);
	void shootMissile(Missile missile[], GameInit data);
	void reloadMissile();
private:
	//default
	const float defaultX = WindowWidth/2 - 62;
	const float defaultY = WindowHeight - 40;
	const float defaultVelX = 13;
	float velX;
};

class Ball
{
public:
	//default
	const float defaultX = WindowWidth/2 - 12.5;
	const float defaultY = 500;
	const float defaultWidth = 25;
	const float defaultHeight = 25;
	const float defaultVelR = 4.5;
	//attribute
	int aura[3] = {0};			//(share)
	float x, y;					//(not share)
	float velX, velY, velR;		//(not share)
	float width, height;		//(share)
	bool destroyed; 			//(not share)
	int type;					//(share)
	//constructor
	Ball();
	int collideBrick(Obj bricks[], int nBricks, Player *player, int cItemDrop[][2], GameInit data);
	void beDestroyed();
	void move(Player* player);
	void toDefault();
	void collideWall(GameInit data);
	void collidePaddle(GameInit data, Paddle paddle);
	void multipleX3();
private:
	const int maxVelR = 25;
	void limitSpeed();
	void typeInvert();
	void typeDestroyer(Obj* brick);
	int typeGhost(float vel);
	void ghostReform();
};

// Item Drop From SpecialBricks
class Item
{
public:
	const float width = 50;
	const float height = 50;
	float x;
	float y;
	int type;
	bool created;
	Item();
	void drop(GameInit data, Paddle* paddle, Ball* ball, Player* player);
	void randomType();
private:
	const float VelY = 4;
	void beTaken(Paddle *paddle, Ball *ball, Player *player);
};

#endif //__GAME_DATA__
