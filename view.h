#ifndef __GAME_VIEW__
#define __GAME_VIEW__

#include "data.h"

class Stage
{
public:
	char msg[100];
	bool running;
	int checkItemDrop[7][2] = {{-1,0}, {-1,0}, {-1,0}, {-1,0}, {-1,0}, {-1,0}, {-1,0}};

	// Ball&Paddle Initialization
	Ball* ball = new Ball[35]; 
	Paddle paddle;

	// Missile Initialization
	Missile* missile = new Missile[10];

	// Item Initailiazation
	Item* item = new Item[7];

	// Constructor
	Stage();

	// Play Game
	bool play(GameInit data, Player* player, Obj bricks[],
	 int nBricks, int nUnbreakable, int level);
protected:
	// Default Brick's Width&Height
	const int defaultBrickWidth = 75;
	const int defaultBrickHeight = 30;
	// Wait
	void wait(GameInit data, Obj bricks[], int nBricks, int nUnbreakable, int level, Player player);

	// Pause
	int pause(GameInit data, int level);

	// Function Check Item Drop 
	void dropItem(GameInit data, Player* player, Obj bricks[]);

	// Function Draw Everything in Stage
	void drawAll(GameInit data, Obj bricks[],
	 int nBricks, int nUnbreakable, int level);

	// Function Set Brick's form
	virtual void setBrick() = 0;
};

class LevelOne : public Stage
{
public:
	// Number of Bricks
	const int nBricks = 80;

	// Number of Unbreakable Brick
	const int nUnbreakable = 0;

	// Level (integer)
	const int level = 1;

	// Create Bricks Object
	Obj bricks[80];
private:
	// Function Set Brick's form
	void setBrick();
};

class LevelTwo : public Stage
{
public:
	// Number of Bricks
	const int nBricks = 82;

	// Number of Unbreakable Bricks
	const int nUnbreakable = 8;

	// Level (integer)
	const int level = 2;

	// Create Bricks Object
	Obj bricks[90];
private:
	// Function Set Brick's form
	void setBrick();
};

class LevelThree : public Stage
{
public:
	// Number of Bricks
	const int nBricks = 86;

	// Number of Unbreakable Bricks
	const int nUnbreakable = 14;

	// Level (integer)
	const int level = 3;

	// Create Bricks Object
	Obj bricks[100];
private:
	// Function Set Brick's form
	void setBrick();
};

class Menu
{
public:
	// Choice for Choose Menu
	int choice;
	bool running;
	// Create Player
	Player player;

	// Constructor (set Choice and running)
	Menu();

	// First Page before Menu
	void enterGame(GameInit data);

	// Loop Control Sequence in Game
	void mainLoop(GameInit data);

	// Main Menu
	int mainMenu(GameInit data);

	// Show Score Board
	int scoreBoard(GameInit data);

	// Game Setting
	int setting(GameInit* data, Player* player);

	// Fade Out
	void fadeOut(GameInit data);
};

#endif //__GAME_VIEW__