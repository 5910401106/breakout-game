Poonyapat Yanvisit 5910401106 (ปุญญพัฒน์ ญาณวิสิฏฐ์)
** You can read THAI Version in "README(THAI).txt" **

- done all topic from Project scoring

How to play
- use key "UP", "DOWN", "LEFT", "RIGHT" and "ENTER" to choose in Menu.
- Setting
	- 1. Mode :
		"Key" 	= use keyboard to control paddle
		"Mouse" = use mouse to control paddle
	- 2. Difficulty Level :
		"Easy" 	  = Slow ball
		"Medium"  = Normal ball
		"Hard"    = Fast ball
	- 3. Start at :
		choose what stage you want to start at.
	- 4. Paddle skin :
		choose your Paddle skin.
	- 5. Sound :
		"On"	= Allow Sound
		"Off"	= Mute Sound
	- 6. Music :
		"On"	= Allow Music
		"Off"	= Mute Music
- Game
	- Control
		1. if you select "Key" mode or default setting.
			- use "LEFT" and "RIGHT" to control your paddle.
			- if you get "ItemMissile" you can press "SPACEBAR" to shoot the missile.
		2. if you select "Mouse" mode.
			- use your mouse motion to control your paddle.
			- if you get "ItemMissile" you can "Left-Click" to shoot the missile.

		*** WARNING, if you use "Key" mode in "Hard Level", ball's speed will
		    higher than your paddle's speed ***

	- Game System
		- if you die your "Life" will decrease by 1 and if your "Life" is 0, you lose.
		- when you lost, you can check your rank at scoreboard if you are in Top10.
		- you can pause by pressing "ESC" on your keyboard.
		- if you finish the stage you will get more score.
			- stage 1 : point +500
			- stage 2 : point +1200
			- stage 3 : point +2500

	- Item
		There are 3 ranks of item in this game : "Good Item", "Very Good Item" and "Bad Item".
		1. Good Item (Green-White Brick)
			1.1 "Slow Ball" : your ball's speed reduce by 10%.
			1.2 "Longer Pad" : your paddle's width increase by 10%.
			1.3 "Point Multiple" : your score rate when collide is increase 10% and it can
			    stack till you lose.
			1.4 "Inverse Ball" : the special ball that is sometimes fast and sometimes Slow.
		2. Very Good Item (Red-Black Brick / Green-Black Brick)
			2.1 "Destroyer Aura" : when you get this item, the ball will have the gold aura.
			    when it collide the brick, the brick will be destroyed with one-hit.
			2.2 "Life Up" : increase "Life" by 1.
			2.3 "Ghost Aura" : the ball will gain purple aura that can penetrate all bricks which
			    are collided except "Unbreakable Brick" (but the ball will pass through it)
			    and it will be back to normal when collide wall or ceiling
				** WARNING, if you get this item, this aura will replace "Destroyer Aura" **
			2.4 "Multiple Ball (x3)" : the ball will separate into 3 balls and you
			    won't lose if you still have at least one ball.
			2.5 "Revive Aura" : the green aura that help you when your ball drop to
			    the floor. you won't lose your "Life" but your special ball will be
			    back to normal and paddle will be back to default size.
			2.6 "Missile Activated" : you can "Left-click" or press "SPACEBAR" to shoot
			    the missile according to your control mode.
		3. Bad Item (Violet-Black Brick)
			3.1 "Speed Ball" : your ball's speed is increase by 20%.
			3.2 "Default Ball" : your ball and paddle will return to their default states.
			3.3 "The Death" : your "Life" is decreased by 1.

*** if you key your name as "teacher" or "tester", you will get special key for test the system ***
+Teacher/Tester mode+
 press '1' for "longer pad"
 press '2' for "slow ball"
 press '3' for "destroyer aura"
 press '4' for "ghost aura"
 press '5' for "revive aura"
 press '6' for "multiple ball x3"
 press '7' for "missile activate"
 press '8' for "life up"