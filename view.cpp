#include <time.h>
#include "view.h"

extern Event event;

Stage::Stage()
{
	this->running = true;
	// Create First Ball
	ball[0].destroyed = false;
}
void Stage::wait(GameInit data, Obj bricks[], int nBricks, int nUnbreakable, int level, Player player)
{
	// wait before start game
	cpClearScreen();
	this->drawAll(data, bricks, nBricks, nUnbreakable, level);
	if (player.mode)
		cpDrawText(100, 255, 255, WindowWidth/2, WindowHeight*0.75, "Click to Start", data.eightBitFontM, 1);
	else
		cpDrawText(100, 255, 255, WindowWidth/2, WindowHeight*0.75, "Press to Start", data.eightBitFontM, 1);
	cpSwapBuffers();
	while(1){
		// click/enter if ready
		while(cbPollEvent(&event)){
			if (event.type == KEYUP && !player.mode && (event.key.keysym.sym == K_RETURN ||event.key.keysym.sym == K_SPACE))
				return;
			else if (event.button.type == M_UP && player.mode)
				return;
		}
	}
}
void Stage::dropItem(GameInit data, Player* player, Obj bricks[])
{
	// check "checkItemDrop (is ItemBrick collide?)"
	for (int i = 0; i < 7; i++){
		// if ItemBrick's Collide
		if (this->checkItemDrop[i][0]+1 && checkItemDrop[i][1]){
			// find Item[] space for add new Item
			for (int j = 0; j < 7; j++){
				// if Item's not exist
				if (!item[j].created){
					// Creating item
					item[j].created = true;
					item[j].x = bricks[this->checkItemDrop[i][0]].x;
					item[j].y = bricks[this->checkItemDrop[i][0]].y;
					item[j].type = checkItemDrop[i][1];
					item[j].randomType();

					// Ready to Create More Item
					checkItemDrop[i][0] = -1;
					checkItemDrop[i][1] = 0;
					break;
				}
			}
		}
	}
	// Item will Drop If it's Created
	for (int i = 0; i < 7 ; i++){
		if (item[i].created)
			//item drop and get, Affect to paddle, ball and player
			item[i].drop(data, &paddle, ball, player);
	}
}
bool Stage::play(GameInit data, Player* player, Obj bricks[], int nBricks, int nUnbreakable, int level)
{

	//PaddleSkin & BallSpd
	paddle.skin = player->padSkin;
	ball[0].velR *= player->difficultyLevel+1;
	// About Brick
	this->setBrick();
	int hitBricks = 0;
	// setRandomItemType
	if (player->mode){
		SDL_WarpMouse(WindowWidth/2, 600);
	}
	srand((unsigned int)clock());
	// start BGM
	if (data.music){
		cpPlaySound(data.toGameSound);
		Mix_PlayMusic(data.bgm,-1);
	}
	// wait before start
	this->wait(data, bricks, nBricks, nUnbreakable, level, *player);
	// start
	while (this->running){
		// clear screen
		cpClearScreen();

		// draw All Obj
		this->drawAll(data, bricks, nBricks, nUnbreakable, level);

		// Display Text
		sprintf(msg, "Point: %5d Life: %d BallSpd: %.1f",
		 player->score, player->life,ball->velR);
		cpDrawText(255, 0, 255, 3, 3, msg, data.eightBitFontS, 0);

		// if Collide Bottom Edge
		ball[0].beDestroyed();

		// am I Die?
		for (int i = 0; i < 35; i++){
			if (!ball[i].destroyed)
				break;
			// if lose all balls
			else if (i == 19){
				if (data.sound)
					cpPlaySound(data.deadSound);
				player->die();
				paddle.toDefault();
				ball[0].toDefault();
				ball[0].velR *= player->difficultyLevel+1;
				if (player->life){
					this->wait(data, bricks, nBricks, nUnbreakable, level, *player);
					SDL_WarpMouse(WindowWidth/2, 600);
				}
			}
		}

		// if no Life = Lose
		if (!player->life){
			return player->lose(data);
		}

		// Get an event
		while (cbPollEvent(&event)){
			// Exit
			if (event.type == QUIT)
				exit(true);
			// Pause
			else if (event.type == KEYUP && event.key.keysym.sym == K_ESCAPE){
				switch (this->pause(data, level)){
					case 1:
						return false;
					case 2:
						exit(true);
				}
			}
			// Mouse
			else if (event.button.type == M_UP && player->mode)
				paddle.shootMissile(missile, data);
			// Keyboard
			if (!player->mode)
				paddle.getEvent(missile, data);

			// for tester (teacher mode)
			if (event.type == KEYUP && (!strcmp(player->name,"teacher") || !strcmp(player->name,"tester"))){
				if (event.key.keysym.sym == '1') paddle.width*=1.1;
				else if (event.key.keysym.sym == '2'){
					for (int i = 0; i < 35; i++){
						if (!ball[i].destroyed){
							ball[i].velR*=0.9;
						}
					}
				}
				else if (event.key.keysym.sym == '3'){
					for (int i = 0; i < 35; i++){
						if (!ball[i].destroyed){
							ball[i].aura[0] = 1;
						}
					}
				}
				else if (event.key.keysym.sym == '4'){
					for (int i = 0; i < 35; i++){
						if (!ball[i].destroyed){
							ball[i].aura[0] = 2;
						}
					}
				}
				else if (event.key.keysym.sym == '5'){
					player->revivable = true;
					for (int i = 0; i < 35; i++){
						if (!ball[i].destroyed){
							ball[i].aura[1] = 1;
						}
					}
				}
				else if (event.key.keysym.sym == '6') ball[0].multipleX3();
				else if (event.key.keysym.sym == '7') paddle.missileActivate = true;
				else if (event.key.keysym.sym == '8') player->life++;
			}
		}

		// missile shooting
		hitBricks += missile[0].shoot(bricks, nBricks+nUnbreakable, data, player);

		// missile Reload
		paddle.reloadMissile();

		//end stage
		if (hitBricks == nBricks){
			player->revivable = false;
			player->pointMultiple = 1;
			if (data.sound)
				cpPlaySound(data.finishStageSound);
			cpSwapBuffers();
			cpDelay(500);
			return true;
		}

		// Check Collide
		for (int i = 0; i < 35; i++){
			if (!ball[i].destroyed){
				ball[i].collideWall(data);
				hitBricks += ball[i].collideBrick(bricks, nBricks+nUnbreakable,
				 player, checkItemDrop, data);
				ball[i].collidePaddle(data, paddle);
			}
		}

		// Is item drop
		this->dropItem(data, player, bricks);

		// Move
		ball[0].move(player);
		paddle.move(*player);

		// Swap
		cpSwapBuffers();

		// Delay
		cpDelay(15);
	}
}
int Stage::pause(GameInit data, int level)
{
	bool pausing = true;
	int choice = 0;
	if (data.music)
		Mix_PauseMusic();
	while (pausing){
		// ClearScreen
		cpClearScreen();
		// Draw BG
		cpDrawTexture(255, 255, 255, 0, 0, WindowWidth, WindowHeight, data.bgStageTexture[level-1]);
		cpDrawTexture(255, 255, 255, 0, 0, WindowWidth, WindowHeight, data.satinTexture);

		// Show Choice
		cpDrawText(choice==0?100:255, 255, 255, WindowWidth/2, WindowHeight*0.25,
		 "Return to Game", data.eightBitFontM, 1);
		cpDrawText(choice==1?100:255, 255, 255, WindowWidth/2, WindowHeight*0.50,
		 "Main Menu", data.eightBitFontM, 1);
		cpDrawText(choice==2?100:255, 255, 255, WindowWidth/2, WindowHeight*0.75,
		 "Exit", data.eightBitFontM, 1);
		// get Event
		while (cbPollEvent(&event)){
			// exit
			if (event.type == QUIT)
				exit(true);
			// choose choice
			if (event.type == KEYUP){
				if (event.key.keysym.sym == K_DOWN && choice < 2){
					choice++;
				}
				else if (event.key.keysym.sym == K_UP && choice > 0){
					choice--;
				}
				else if (event.key.keysym.sym == K_RETURN){
					if (choice == 0)
						if (data.music)
							Mix_ResumeMusic();
					else
						if (data.music)
							Mix_HaltMusic();
					return choice;
				}
			}
		}
		cpSwapBuffers();
	}
}
void Stage::drawAll(GameInit data, Obj bricks[], int nBricks, int nUnbreakable, int level)
{
	// Draw Background
	cpDrawTexture(255, 255, 255,
	 0, 0, WindowWidth, WindowHeight, data.bgStageTexture[level-1]);

	// Draw Paddle
	cpDrawTexture(255, 255, 255,
	 paddle.x, paddle.y, paddle.width, paddle.height, data.paddleTexture[paddle.skin]);

	// Draw Missile If Activate
	if (paddle.missileActivate){
		cpDrawTexture(255, 255, 255,
		 paddle.x, paddle.y-10, 20, 50, data.missileTexture);
		cpDrawTexture(255, 255, 255,
		 paddle.x+paddle.width-20, paddle.y-10, 20, 50, data.missileTexture);
	}

	// Draw Missile
	for (int i = 0; i < 10; i++){
		if (missile[i].fired){
			cpDrawTexture(255, 255, 255,
			 missile[i].x, missile[i].y, missile[i].width, missile[i].height, data.missileTexture);
		}
	}

	// Draw Ball
	for (int i = 0; i < 35; i++){
		if (!ball[i].destroyed){
			cpDrawTexture(255, 255, 255,
			 ball[i].x, ball[i].y, ball[i].width, ball[i].height, data.ballTexture[ball[i].type]);
			if (ball[i].aura[1] > 0){
				cpDrawTexture(255, 255, 255,
				 ball[i].x-8, ball[i].y-8, ball[i].width+16, ball[i].height+16, data.ballAura[ball[i].aura[1]+1]);
			}
			if (ball[i].aura[0] > 0){
				cpDrawTexture(255, 255, 255,
				 ball[i].x-5, ball[i].y-5, ball[i].width+10, ball[i].height+10, data.ballAura[ball[i].aura[0]-1]);
			}
		}
	}

	// draw Bricks
	for (int i = 0; i < nBricks + nUnbreakable; i++){
		if (bricks[i].form+1){
			// Special Bricks
			if (bricks[i].form < 5)
				cpDrawTexture(255, 255, 255,
			 	 bricks[i].x, bricks[i].y, bricks[i].width,
			 	  bricks[i].height, data.brickTexture[bricks[i].form]);
			// Bricks for Stage 1
			else if (level == 1){
				cpDrawTexture(255, 255, 255,
			 	 bricks[i].x, bricks[i].y, bricks[i].width,
			 	  bricks[i].height, data.brick1Texture[bricks[i].form-5]);
			}
			// Bricks for Stage 2
			else if (level == 2){
				cpDrawTexture(255, 255, 255,
			 	 bricks[i].x, bricks[i].y, bricks[i].width,
			 	  bricks[i].height, data.brick2Texture[bricks[i].form-5]);
			}
			// Bricks for Stage 3
			else if (level == 3){
				cpDrawTexture(255, 255, 255,
			 	 bricks[i].x, bricks[i].y, bricks[i].width,
			 	  bricks[i].height, data.brick3Texture[bricks[i].form-5]);
			}
		}
	}

	// draw Item
	for (int i = 0; i < 7; i++){
		if (item[i].created){
			cpDrawTexture(255, 255, 255, item[i].x, item[i].y,
			 item[i].width, item[i].height, data.itemTexture[item[i].type]);
		}
	}
}
void LevelOne::setBrick()
{
	//form 0-4 = item/ubk
	//form 5 = base
	//form 6-7 = 2hit
	//form 8-10 = 3hit
	//form 11-14 = 4hit
	for (int n = 0, x = 25, y = 60; n < this->nBricks; n++){
		this->bricks[n].width = this->defaultBrickWidth;
		this->bricks[n].height = this->defaultBrickHeight;
		if (x + this->bricks[n].width + 25 > WindowWidth){
			x = 25;
			y += bricks[n].height;
		}
		this->bricks[n].x = x;
		this->bricks[n].y = y;
		this->bricks[n].destroyed = false;
		x += this->bricks[n].width;
		//form itemX
		if (n==1 || n==8 || n==10 || n==19 || n==23 ||
		 n==26 || n==32 || n==37 || n==42 || n==47 ||
		 n==53 || n==56 || n==60 || n==69 || n==71 ||
		 n==78)
			this->bricks[n].form = 3;
		//form itemS
		else if (n==2 || n==7 || n==30 || n==34 || n==35 ||
		 n==39 || n==40 || n==44 || n==45 || n==49)
			this->bricks[n].form = 1;
		//form bad
		else if (n>=3 && n<=6)
			this->bricks[n].form = 4;
		//form 2hit
		else if (n==0 || n==9 || n==11 || n==18 || n==22 ||
		 n==24 || n==25 || n==27 || n==33 || n==36 ||
		 n==43 || n==46 || n==52 || n==54 || n==55 ||
		 n==57 || n==61 || n==68 || n==70 || n==79)
			this->bricks[n].form = 6;
		//form base
		else
			this->bricks[n].form = 5;
	}
}
void LevelTwo::setBrick()
{
	//form 0-4 = item/ubk
	//form 5 = base
	//form 6-7 = 2hit
	//form 8-10 = 3hit
	//form 11-14 = 4hit
	for (int n = 0, x = 25, y = 60; n < this->nBricks + 8; n++){
		this->bricks[n].width = this->defaultBrickWidth;
		this->bricks[n].height = this->defaultBrickHeight;
		if (x + this->bricks[n].width + 25 > WindowWidth){
			x = 25;
			y += bricks[n].height;
		}
		this->bricks[n].x = x;
		this->bricks[n].y = y;
		this->bricks[n].destroyed = false;
		x += this->bricks[n].width;
		// form ItemX
		if (n==0 || n==9 || n==24 || n==25 || n==32 ||
		 n==37 || n==42 || n==47 || n==52 || n==57 ||
		 n==80 || n==89)
			this->bricks[n].form = 3;
		// form ItemS
		else if (n==1 || n==8 || n==30 || n==34 || n==35 ||
		 n==39 || n==54 || n==55)
			this->bricks[n].form = 1;
		// form ItemBad
		else if (n==3 || n==4 || n==5 || n==6 || n==64 ||
		 n==65)
			this->bricks[n].form = 4;
		// form Unbreakable
		else if (n==43 || n==44 || n==45 || n==46 || n==81 ||
		 n==82 || n==87 || n==88)
			this->bricks[n].form = 0;
		// form 3 Hit
		else if (n==2 || n==7 || n==11 || n==18 || n==20 ||
		 n==22 || n==23 || n==26 || n==27 || n==29 ||
		 n==33 || n==36 || n==53 || n==56 || n==60 ||
		 n==62 || n==63 || n==66 || n==67 || n==69 ||
		 n==71 || n==78)
			this->bricks[n].form = 8;
		// form 2 Hit
		else if (n==12 || n==13 || n==14 || n==15 || n==16 ||
		 n==17 || n==21 || n==28 || n==31 || n==38 ||
		 n==41 || n==48 || n==51 || n==58 || n==61 ||
		 n==68 || n==73 || n==74 || n==75 || n==76)
			this->bricks[n].form = 6;
		else
			this->bricks[n].form = 5;
	}
}
void LevelThree::setBrick()
{
	//form 0-4 = item/ubk
	//form 5 = base
	//form 6-7 = 2hit
	//form 8-10 = 3hit
	//form 11-14 = 4hit
	for (int n = 0, x = 25, y = 60; n < this->nBricks + 14; n++){
		this->bricks[n].width = this->defaultBrickWidth;
		this->bricks[n].height = this->defaultBrickHeight;
		if (x + this->bricks[n].width + 25 > WindowWidth){
			x = 25;
			y += bricks[n].height;
		}
		this->bricks[n].x = x;
		this->bricks[n].y = y;
		this->bricks[n].destroyed = false;
		x += this->bricks[n].width;
		// form ItemX
		if (n==0 || n==9 || n==20 || n==22 || n==27 ||
			n==29 || n==74 || n==75 || n==90 || n==99)
			this->bricks[n].form = 3;
		// form ItemS
		else if (n==24 || n==25 || n==54 || n==55 || n==60 ||
		 n==61 || n==68 || n==69)
			this->bricks[n].form = 2;
		// form ItemBad
		else if (n==50 || n==51 || n==58 || n==59 || n==84 ||
		 n==85)
			this->bricks[n].form = 4;
		// form Unbreakable
		else if (n==12 || n==13 || n==16 || n==17 || n==23 ||
		 n==26 || n==33 || n==36 || n==70 || n==71 ||
		 n==72 || n==77 || n==78 || n==79)
			this->bricks[n].form = 0;
		// form 4 Hit
		else if (n>=1 && n<=8 || n==21 || n==28 || n==32 ||
		 n==34 || n==35 ||  n==37 || n>=91 && n<=98)
			this->bricks[n].form = 11;
		// form 3 Hit
		else if (n==30 || n==39 || n==40 || n==41 || n==48 ||
		 n==49 || n==52 || n==57 || n==62 || n==67 ||
		 n==73 || n==76 || n>=80 && n<=89 && n!=84 && n!=85)
			this->bricks[n].form = 8;
		// form 2 Hit
		else if (n==10 || n==19 || n==31 || n==38 ||
		 n>=42 && n<=47 || n==53 || n==56 || n>=63 && n<=66)
			this->bricks[n].form = 6;
		else
			this->bricks[n].form = 5;
	}
}


Menu::Menu()
{
	choice = 0;
	running = true;
}
void Menu::mainLoop(GameInit data)
{
	// Create Player
	Player player;

	// choice for control the loop
	int choice;

	// complete the stage if it's true
	bool next = false;

	// first page
	this->enterGame(data);
	this->fadeOut(data);

	// main loop
	while (choice = this->mainMenu(data)){
		// Play
		if (choice == 1){
			// Add Player name before play game
			if (player.addName(data)){
				this->fadeOut(data);
				// if use mouse mode, lock mouse in game window
				if (player.mode)
					SDL_WM_GrabInput(SDL_GRAB_ON);
				// stage 1
				if (player.startAt==1){
					this->fadeOut(data);
					LevelOne levelOne;
					if (next = levelOne.play(data, &player, levelOne.bricks, levelOne.nBricks, levelOne.nUnbreakable, levelOne.level)){
						if (data.music)
							Mix_HaltMusic();
						player.score+=500;
					}
				}
				// stage 2
				if (player.startAt==2 || next){
					this->fadeOut(data);
					next = false;
					LevelTwo levelTwo;
					if (next = levelTwo.play(data, &player, levelTwo.bricks, levelTwo.nBricks, levelTwo.nUnbreakable, levelTwo.level)){
						if (data.music)
							Mix_HaltMusic();
						player.score+=1200;
					}
				}
				// stage 3
				if (player.startAt==3 || next){
					this->fadeOut(data);
					next = false;
					LevelThree levelThree;
					if (next = levelThree.play(data, &player, levelThree.bricks, levelThree.nBricks, levelThree.nUnbreakable, levelThree.level)){
						if (data.music)
							Mix_HaltMusic();
						player.score+=2500;
						player.lose(data);
					}
				}
				this->fadeOut(data);
				if (data.music)
					Mix_HaltMusic();
			}
			// End Game
			player.toDefault();
		}
		// Show ScoreBoard
		else if (choice == 2){
			this->fadeOut(data);
			this->scoreBoard(data);
			this->fadeOut(data);
		}
		// Setting
		else if (choice == 3){
			this->fadeOut(data);
			this->setting(&data, &player);
			this->fadeOut(data);
		}
		// free player mouse
		SDL_WM_GrabInput(SDL_GRAB_OFF);
	}
}
int Menu::mainMenu(GameInit data)
{
	// start
	while (this->running){
		// clear Screen
		cpClearScreen();
		// Draw BG
		cpDrawTexture(255, 255, 255, 0, 0,
		 WindowWidth, WindowHeight, data.bgMenuTexture);
		// Draw Choice
		cpDrawText(choice==0?100:255, 255, 255, WindowWidth/2, WindowHeight*0.2, "New Game", data.eightBitFontM, 1);
		cpDrawText(choice==1?100:255, 255, 255, WindowWidth/2, WindowHeight*0.4, "Score Board", data.eightBitFontM, 1);
		cpDrawText(choice==2?100:255, 255, 255, WindowWidth/2, WindowHeight*0.6, "Setting", data.eightBitFontM, 1);
		cpDrawText(choice==3?100:255, 255, 255, WindowWidth/2, WindowHeight*0.8, "Exit", data.eightBitFontM, 1);
		// getEvent
		while (cbPollEvent(&event)){
			// exit
			if (event.key.keysym.sym == K_ESCAPE || event.type == QUIT)
				exit(true);
			// choice selection
			else if (event.type == KEYUP){
				if (event.key.keysym.sym == K_UP && this->choice > 0){
					if (data.sound)
						cpPlaySound(data.selectorSound);
					this->choice--;
				}
				else if (event.key.keysym.sym == K_DOWN && this->choice < 3){
					if (data.sound)
						cpPlaySound(data.selectorSound);
					this->choice++;
				}
				else if (event.key.keysym.sym == K_RETURN){
					if (data.sound)
						cpPlaySound(data.enterSound);
					if (this->choice < 3)
						return this->choice+1;
					if (this->choice == 3)
						exit(true);
				}
			}
		}
		// update
		cpSwapBuffers();
	}
}
void Menu::enterGame(GameInit data)
{
	//Fade in
	cpDrawTexture(255, 255, 255, 0, 0,
	 WindowWidth, WindowHeight, data.bgMenuTexture);
	cpSwapBuffers();
	cpDrawTexture(255, 255, 255, 0, 0,
	 WindowWidth, WindowHeight, data.bgMenuTexture);
	for (int i = 0; i < 100; i++){
		cpDrawTexture(255, 255 ,255 , 175, 100,
		 450, 200, data.gameFadeTexture);
		cpDrawTexture(255, 255 ,255 , 250, 400,
		 300, 100, data.pressEnterFadeTexture);
		cpSwapBuffers();
	}
	//enterGame
	while (this->running){
		// clear Screen
		cpClearScreen();
		cpDrawTexture(255, 255, 255, 0, 0,
		 WindowWidth, WindowHeight, data.bgMenuTexture);
		cpDrawTexture(255, 255 ,255 , 175, 100,
		 450, 200, data.gameNameTexture);
		cpDrawTexture(255, 255 ,255 , 250, 400,
		 300, 100, data.pressEnter[(int)(clock()/CLOCKS_PER_SEC)%2]);
		// getEvent
		while (cbPollEvent(&event)){
			if (event.type == KEYUP && event.key.keysym.sym == K_RETURN){
				cpPlaySound(data.enterSound);
				return;
			}
			else if (event.key.keysym.sym == K_ESCAPE || event.type == QUIT){
				exit(1);
			}
		}
		cpDelay(10);
		cpSwapBuffers();
	}
}
int Menu::scoreBoard(GameInit data)
{
	// write 2 buffer for fading out when exit the page;
	for (int i = 0; i<2 ; i++){
		// clear Screen
		cpClearScreen();

		// Draw Background
		cpDrawTexture(255, 255, 255, 0, 0,
		 WindowWidth, WindowHeight, data.bgMenuTexture);
		cpDrawTexture(255, 255, 255, 0, 0,
		 WindowWidth, WindowHeight, data.satinTexture);

		// Draw Head
		cpDrawText(0, 255, 255, 50, 30, "NAME", data.eightBitFontM, 0);
		cpDrawText(0, 255, 255, 280, 30, "SCORE", data.eightBitFontM, 0);
		cpDrawText(055, 255, 255, 450, 30, "MAXSPD", data.eightBitFontM, 0);
		cpDrawText(055, 255, 255, 650, 30, "TIME", data.eightBitFontM, 0);

		// Draw ScoreBoard
		FILE* board = fopen("./data/scrBoard", "r");
		Eplayer ePlayer;
		char temp[20];
		for (int i = 0; i < 10; i++){
			fscanf(board, "%[^\t]\t%d\t%f\t%[^\n]\n", &ePlayer.name, &ePlayer.score, &ePlayer.speed, &ePlayer.timePlay);
			cpDrawText(i%2?220:255, i%2?220:255, i%2?220:255, 50, WindowHeight*(i+3)/14, ePlayer.name, data.eightBitFontS, 0);
			sprintf(temp, "%10d", ePlayer.score);
			cpDrawText(i%2?220:255, i%2?220:255, i%2?220:255, 370-120, WindowHeight*(i+3)/14, temp, data.eightBitFontS, 0);
			sprintf(temp, "%10.1f", ePlayer.speed);
			cpDrawText(i%2?220:255, i%2?220:255, i%2?220:255, 600-165, WindowHeight*(i+3)/14, temp, data.eightBitFontS, 0);
			cpDrawText(i%2?220:255, i%2?220:255, i%2?220:255, 600+30, WindowHeight*(i+3)/14, ePlayer.timePlay, data.eightBitFontS, 0);
		}
		cpSwapBuffers();
	}

	// getEvent
	while(1){
		while(cbPollEvent(&event)){
			// Exit
			if (event.type == QUIT)
				exit(true);
			// Back
			else if (event.type == KEYUP && (event.key.keysym.sym == K_ESCAPE || event.key.keysym.sym == K_RETURN)){
				if (data.sound)
					cpPlaySound(data.enterSound);
				return 1;
			}
		}
	}
}
int Menu::setting(GameInit* data, Player* player)
{
	int choiceTopic = 0;
	int choiceSelect[6] = {player->mode, player->difficultyLevel, player->startAt-1, player->padSkin, data->sound, data->music};
	bool setting = true;
	while (setting){
		cpClearScreen();
		// Draw BG
		cpDrawTexture(255, 255, 255, 0, 0,
		 WindowWidth, WindowHeight, data->bgMenuTexture);

		// Draw Setting Topic
		cpDrawText(255, 255, 255, WindowWidth/2, 30, "Setting", data->eightBitFontM, 1);
		cpDrawText(255, 255, 255, 50, WindowHeight*0.10, "Mode : ", data->eightBitFontM, 0);
		cpDrawText(255, 255, 255, 50, WindowHeight*0.20, "Difficulty Level : ", data->eightBitFontM, 0);
		cpDrawText(255, 255, 255, 50, WindowHeight*0.30, "Start at : ", data->eightBitFontM, 0);
		cpDrawText(255, 255, 255, 50, WindowHeight*0.40, "Paddle Skin : ", data->eightBitFontM, 0);
		cpDrawText(255, 255, 255, 50, WindowHeight*0.50, "Sound : ", data->eightBitFontM, 0);
		cpDrawText(255, 255, 255, 50, WindowHeight*0.60, "Music : ", data->eightBitFontM, 0);

		// Draw Setting Choice
		if (choiceTopic<6)
			cpDrawTexture(255, 255, 255, 20, WindowHeight*0.1*(choiceTopic+1)+20, WindowWidth-50, WindowHeight*0.1, data->selectionTexture[1]);
		// Mode
		cpDrawText(choiceSelect[0]==0?100:230, 230, 230, WindowWidth*0.52, WindowHeight*0.10, "KEY", data->eightBitFontM, 0);
		cpDrawText(choiceSelect[0]==1?100:230, 230, 230, WindowWidth*0.7, WindowHeight*0.10, "MOUSE", data->eightBitFontM, 0);

		// Difficulty Level
		cpDrawText(choiceSelect[1]==0?100:230, 230, 230, WindowWidth*0.45, WindowHeight*0.20, "Easy", data->eightBitFontM, 0);
		cpDrawText(choiceSelect[1]==1?100:230, 230, 230, WindowWidth*0.6, WindowHeight*0.20, "Medium", data->eightBitFontM, 0);
		cpDrawText(choiceSelect[1]==2?100:230, 230, 230, WindowWidth*0.8, WindowHeight*0.20, "Hard", data->eightBitFontM, 0);

		// Start at
		cpDrawText(choiceSelect[2]==0?100:230, 230, 230, WindowWidth*0.4, WindowHeight*0.30, "Level 1", data->eightBitFontM, 0);
		cpDrawText(choiceSelect[2]==1?100:230, 230, 230, WindowWidth*0.6, WindowHeight*0.30, "Level 2", data->eightBitFontM, 0);
		cpDrawText(choiceSelect[2]==2?100:230, 230, 230, WindowWidth*0.8, WindowHeight*0.30, "Level 3", data->eightBitFontM, 0);

		// Paddle Skin
		cpDrawTexture(255, 255, 255, WindowWidth*0.58, WindowHeight*0.455, 150, 30, data->paddleTexture[choiceSelect[3]]);
		cpDrawText(choiceTopic==3?100:230, 230, 230, WindowWidth*0.5, WindowHeight*0.4, "<<              >>", data->eightBitFontM, 0);
		// Sound
		cpDrawText(choiceSelect[4]==0?100:230, 230, 230, WindowWidth*0.55, WindowHeight*0.50, "OFF", data->eightBitFontM, 0);
		cpDrawText(choiceSelect[4]==1?100:230, 230, 230, WindowWidth*0.75, WindowHeight*0.50, "ON", data->eightBitFontM, 0);
		// Music
		cpDrawText(choiceSelect[5]==0?100:230, 230, 230, WindowWidth*0.55, WindowHeight*0.60, "OFF", data->eightBitFontM, 0);
		cpDrawText(choiceSelect[5]==1?100:230, 230, 230, WindowWidth*0.75, WindowHeight*0.60, "ON", data->eightBitFontM, 0);
		// Back Button
		cpDrawTexture(255, 255, 255, WindowWidth*0.45, WindowHeight*0.8, 80, 80, data->selectionTexture[choiceTopic==6?1:0]);
		cpDrawText(choiceTopic==6?100:255, 255, 255, WindowWidth*0.5, WindowHeight*0.85, "Back", data->eightBitFontM, 1);

		// getEvent
		while(cbPollEvent(&event)){
			// Exit
			if (event.type == QUIT)
				exit(true);
			// Back
			else if (event.type == KEYUP){
				if (event.key.keysym.sym == K_ESCAPE){
					return 1;
				}
				else if (event.key.keysym.sym == K_RETURN && choiceTopic == 6){
					if (data->sound)
						cpPlaySound(data->enterSound);
					player->getSettingData(choiceSelect);
					data->sound = choiceSelect[4];
					data->music = choiceSelect[5];
					return 1;
				}
				else if (event.key.keysym.sym == K_RETURN && choiceTopic != 6){
					if (data->sound)
						cpPlaySound(data->selectorSound);
					choiceTopic = 6;
				}
				else if (event.key.keysym.sym == K_UP && choiceTopic > 0){
					if (data->sound)
						cpPlaySound(data->selectorSound);
					choiceTopic--;
				}
				else if (event.key.keysym.sym == K_DOWN && choiceTopic < 6){
					if (data->sound)
						cpPlaySound(data->selectorSound);
					choiceTopic++;
				}
				else if (event.key.keysym.sym == K_LEFT && choiceSelect[choiceTopic] > 0 && choiceTopic!=3){
					if (data->sound)
						cpPlaySound(data->selectorSound);
					choiceSelect[choiceTopic]--;
				}
				else if (event.key.keysym.sym == K_LEFT && choiceTopic==3){
					if (data->sound)
						cpPlaySound(data->selectorSound);
					choiceSelect[choiceTopic]--;
					if (choiceSelect[choiceTopic] < 0)
						choiceSelect[choiceTopic] = 4;
				}
				else if (event.key.keysym.sym == K_RIGHT){
					if ((choiceTopic == 0 || choiceTopic == 4 || choiceTopic == 5) && choiceSelect[choiceTopic] < 1){
						if (data->sound)
							cpPlaySound(data->selectorSound);
						choiceSelect[choiceTopic]++;
					}
					else if ((choiceTopic == 1 || choiceTopic == 2) && choiceSelect[choiceTopic] < 2){
						if (data->sound)
							cpPlaySound(data->selectorSound);
						choiceSelect[choiceTopic]++;
					}
					else if (choiceTopic == 3){
						if (data->sound)
							cpPlaySound(data->selectorSound);
						choiceSelect[choiceTopic]++;
						choiceSelect[choiceTopic]%=5;
					}
				}
			}
		}


		cpSwapBuffers();
	}
}
void Menu::fadeOut(GameInit data)
{
	for (int i = 0; i < 50 ; i++){
		cpDrawTexture(255, 255, 255, 0, 0,
		 WindowWidth, WindowHeight, data.satinTexture);
		cpDelay(10);
		cpSwapBuffers();
	}
}
