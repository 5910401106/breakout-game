CC = gcc.exe
CXX = g++.exe

CFLAGS = -std=c++11 -I/mingw32/include
LDFLAGS = -L/mingw32/lib -lSDLmain -lSDL -lSDL_image -lSDL_mixer -lSDL_ttf -lopengl32 -lglu32

all: breakout.exe

cp_functions.o: cp_functions.cpp
	$(CXX) $(CFLAGS) -c cp_functions.cpp

data.o: data.cpp
	$(CXX) $(CFLAGS) -c data.cpp

view.o: view.cpp
	$(CXX) $(CFLAGS) -c view.cpp

main.o: main.cpp
	$(CXX) $(CFLAGS) -c main.cpp

breakout.exe: cp_functions.o data.o view.o main.o
	$(CXX) -o breakout.exe cp_functions.o data.o view.o main.o $(LDFLAGS)

new: clean cp_functions.o data.o view.o main.o breakout.exe

clean:
	rm -f *.o breakout.exe
